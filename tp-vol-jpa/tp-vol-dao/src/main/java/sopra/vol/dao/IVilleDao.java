package sopra.vol.dao;

import sopra.vol.Ville;

public interface IVilleDao extends IDao<Ville, Long> {
	Ville FindByNom (String nom);
}
